PROJECT = watch

.PHONY: all build link flash clean

all: build link flash clean

build: $(PROJECT).c
	avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328p -c -o $(PROJECT).o $(PROJECT).c

link: $(PROJECT).o
	avr-gcc -mmcu=atmega328p $(PROJECT).o -o $(PROJECT).elf
	avr-objcopy -O ihex -R .eeprom $(PROJECT).elf $(PROJECT).hex

flash: $(PROJECT).hex
	avrdude -F -V -c arduino -p ATMEGA328P -P /dev/ttyACM0 -b 115200 -U flash:w:$(PROJECT).hex

clean:
	rm $(PROJECT).o
	rm $(PROJECT).elf
	rm $(PROJECT).hex


