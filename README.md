# avr-watch

a digital watch written in AVR C, for ATMEGA328P (Arduino UNO).

![](img/video.webm)

## building

1. connect board via USB

2. run `make`


## using

press button once to adjust hours, again for minutes, again for seconds, then again to return.

when in adjust mode, turn potentiometer right to increment, left to decrement, middle to hold.

## wiring

```
connect 1602 LCD: {
	D0-D7 -> UNO D0-D7,
	RS -> UNO D11,
	EN -> UNO D12,
	A -> 5V
	VSS -> 5V
	VDD -> GND
	V0 -> 5V through 2K ohm resistor
}

connect pushbutton to GND through 10K ohm resistor, and to UNO D8.

connect potentiometer output to UNO A0.
```