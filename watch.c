/* digital watch */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include "lcd.h"
#include "time.h"


/* Target Timer Count = (1 / Target Frequency) / (1 / Timer Clock Frequency) - 1 */
/* 1 is subtracted because timer starts ticking at 0 */
/* target frequency is once every millisecond */
const uint16_t TIMER_COUNT_TARGET = 15999UL;

volatile uint8_t btn_clicked = 0;

ISR(PCINT0_vect) {
	btn_clicked = 1;
}

int main() {
	volatile uint16_t ms_counter = 0;
	volatile uint8_t time_set_pos = 0;
	volatile uint8_t running = 1;
	volatile time_t time = {0};
	char time_display[9];

	/* pins init */
	DDRD  = 0b11111111;  /* set D0-D7 as output */
	DDRB |= 0b00111000;  /* high 2 bits (crystal pins) must be left untouched */
	
	/* analog-to-digital input init */
	/* set ADC clock to 125 KHz using prescalers (by setting low 3 bits of ADCSRA to 1) */
	/* 125KHz is the max ADC clock frequency possible using prescalers (absolute max is 200KHz) */
	ADCSRA |= ((1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0));
	ADCSRA |= (1<<ADATE);
	/* set ADC reference voltage to AVcc = 5V */
	ADMUX  |= (1<<REFS0);
	ADMUX  &= ~(1<<REFS1);
	/* enable ADC */
	ADCSRA |= (1<<ADEN);
	/* initiate a conversion */
	ADCSRA |= (1<<ADSC); 

	/* timer init */
	/* starts timer at same clock frequency as the cpu */
	TCCR1B |= (1 << CS10);

	lcd_init();

	/* interrupt setup */
	PCICR |= _BV(PCIE0);
	PCMSK0 |= _BV(PCINT0);
	sei();

	while (1) {
		if (TCNT1 >= TIMER_COUNT_TARGET) {
			ms_counter++;
			if (ms_counter >= 1000) {
				if (btn_clicked) {
					/* only responds to button presses once per second, in effect a good debounce */
					running = 0;
					time_set_pos++;
					if (time_set_pos > 3) {
						lcd_send_cmd(LCD_CMD_DISPLAY_ON_CURSOR_OFF);
						time_set_pos = 0;
						running = 1;
					}
					btn_clicked = 0;
				}
				if (running) {
					time_tick(&time, 1);
				} else {
					lcd_send_cmd(LCD_CMD_DISPLAY_ON_CURSOR_ON);
					uint16_t A0 = ADCW;
					if (A0 > 768) {
						if (time_set_pos == 1) {
							time_tick(&time, 3600);
						} else if (time_set_pos == 2) {
							time_tick(&time, 60);
						} else if (time_set_pos == 3) {
							time_tick(&time, 1);
						}
					}
					if (A0 < 256) {
						if (time_set_pos == 1) {
							time_untick(&time, 3600);
						} else if (time_set_pos == 2) {
							time_untick(&time, 60);
						} else if (time_set_pos == 3) {
							time_untick(&time, 1);
						}
					}
				}
				snprintf(time_display, 9, "%02u:%02u:%02u", time.hours, time.minutes, time.seconds);
				lcd_send_cmd(LCD_CMD_CURSOR_HOME);
				lcd_send_str(time_display);
				if (!running) {
					if (time_set_pos == 1) {
						lcd_cursor(0, 1);
					} else if (time_set_pos == 2) {
						lcd_cursor(0, 4);
					} else if (time_set_pos == 3) {
						lcd_cursor(0, 7);
					}
				}
				ms_counter = 0;
			}
			TCNT1 = 0;
		}
	}
}