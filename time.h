struct time {
	uint8_t hours;
	uint8_t minutes;
	uint8_t seconds;
};

typedef	struct time time_t;

void time_tick(volatile time_t * t, uint16_t sec) {
	while (sec >= 60) {
		t->minutes++;
		if (t->minutes >= 60) {
			t->minutes -= 60;
			t->hours++;
		}
		if (t->hours >= 24) {
			t->hours = 0;
		}
		sec -= 60;
	}
	t->seconds += sec;
}

void time_untick(volatile time_t * t, uint16_t sec) {
	while (sec >= 60) {
		t->minutes--;
		if (t->minutes == 255) {
			t->minutes = 59;
			t->hours--;
		}
		if (t->hours == 255) {
			t->hours = 23;
		}
		sec -= 60;
	}
	t->seconds -= sec;
	if (t->seconds > 60) {
		t->seconds = 60 - (256 - t->seconds);
		t->minutes--;
	}
	if (t->minutes == 255) {
		t->minutes =  59;
		t->hours--;
	}
	if (t->hours == 255) {
		t->hours = 23;
	}
}