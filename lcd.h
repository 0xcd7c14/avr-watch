/* datasheet: https://www.openhacks.com/uploadsproductos/eone-1602a1.pdf */

#include <avr/io.h>
#include <util/delay.h>
#include "string.h"

#define PIN_RS PORTB3
#define PIN_EN PORTB4

#define LCD_CMD_CLEAR_DISPLAY 0b00000001
#define LCD_CMD_CURSOR_HOME 0b00000010
#define LCD_CMD_DISPLAY_ON_CURSOR_OFF 0b00001100
#define LCD_CMD_DISPLAY_ON_CURSOR_ON  0b00001110
#define LCD_CMD_MODE_2L_5X8   0b00111000
#define LCD_CMD_CURSOR_LEFT   0b00010000
#define LCD_CMD_CURSOR_RIGHT  0b00010100
/* mode: 0, 0, 1, (4 or 8 bits?), (1 or 2 line?), (5x8 or 5x11 font?), any, any [first choice = 0, second = 1] */

void lcd_send_enable() {
	/* lcd enable pin is triggered on falling edge */
	/* (with at least 140ns between high and low, see Enable Pulse Width in datasheet) */
	PORTB |= _BV(PIN_EN);
	_delay_us(0.14);
	PORTB &= ~_BV(PIN_EN);
}

void lcd_send_cmd(const char cmd) {
	PORTD = cmd;
	PORTB &= ~_BV(PIN_RS);  /* set RS low (command mode) */
	lcd_send_enable();
	_delay_ms(1);
}

void lcd_init() {
	lcd_send_cmd(LCD_CMD_MODE_2L_5X8);
	lcd_send_cmd(LCD_CMD_DISPLAY_ON_CURSOR_OFF);
	lcd_send_cmd(LCD_CMD_CLEAR_DISPLAY);
	lcd_send_cmd(LCD_CMD_CURSOR_HOME);
	lcd_send_cmd(LCD_CMD_MODE_2L_5X8);
}

void lcd_send_data(const char data) {
	PORTD = data;
	PORTB |=  _BV(PIN_RS);  /* set RS high (data mode) */
	lcd_send_enable();
	_delay_ms(1);
}

void lcd_send_str(const char * str) {
	for (const char * itr = str; *itr; ++itr) {
		lcd_send_data(*itr);
	}
}

void lcd_cursor(uint8_t line, uint8_t pos) {
	if (line == 1) {
		pos += 40;
	}
	lcd_send_cmd(LCD_CMD_CURSOR_HOME);
	for (uint8_t i = 0; i < pos; ++i) {
		lcd_send_cmd(LCD_CMD_CURSOR_RIGHT);
	}
}